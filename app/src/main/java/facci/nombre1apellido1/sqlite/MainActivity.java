package facci.nombre1apellido1.sqlite;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    Button Buscar,Guardar,Modificar, Eliminar, ConsultaIndividual;
    EditText Titulo,Edicion,ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         Buscar = (Button) findViewById(R.id.btnBuscar);
         Guardar = (Button) findViewById(R.id.BtnGuardar);
        Modificar = (Button) findViewById(R.id.BtnModificar);
        Eliminar = (Button) findViewById(R.id.BtnEliminar);
        ConsultaIndividual = (Button) findViewById(R.id.BtnConsultaIndividual);

        Titulo = (EditText) findViewById(R.id.TxtTitulo);
        Edicion = (EditText) findViewById(R.id.TxtEdicion);
        ID = (EditText) findViewById(R.id.editTextId);

         Guardar.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 Book registro1 = new Book(Titulo.getText().toString(), Edicion.getText().toString());
                 registro1.save();
                 Toast.makeText(MainActivity.this,"Guardado Correctamente",Toast.LENGTH_SHORT).show();
             }
         });

         Modificar.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Book book = Book.findById(Book.class, Long.parseLong(""));
                 book.title = "Señor De los Anillos"; //modify values
                 book.edition="Second edition";
                 book.save();
             }
         });

        ConsultaIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Book book = Book.findById(Book.class, Long.parseLong(ID.getText().toString()));

                    Titulo.setText(book.getTitle());
                    Edicion.setText(book.getEdition());

            }
        });




        Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
            }
        });

        Eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Book> books = Book.listAll(Book.class);

                Book.deleteAll(Book.class);
            }
        });
    }
}
